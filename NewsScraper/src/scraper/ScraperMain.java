package scraper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import scraper.Headlines.Headline;
import scraper.output.JsonOutput;

public class ScraperMain {

	public static void main(String[] args) {
		// testRSSParser("http://feeds.gawker.com/lifehacker/full");

		// FeedlyParser.getContentDirectly(
		// "feed/http://feeds.gawker.com/lifehacker/vip", 2);

		// AbstractSiteScraper bbc = new BBCScraper();
		// System.out.println(bbc.scrape("http://m.bbc.co.uk/news/uk-31861567"));

		// Scraper.getArticles(Feed.BBC, 3);

		// Determine what do do based on arguments
		// CLI.parseArgs(args);

		// System.out.println(Paths.get("a","b").toAbsolutePath());

		if (args.length < 4) {
			System.out.println("Missing arguments");
			System.exit(0);
		}

		// String company = "AAPL";
		// String fileName = "headlines.txt";
		// String firstDate = "2015-03-10";
		// String secondDate = "2015-02-28";

		String company = args[0];
		String fileName = args[1];
		String firstDate = args[2];
		String secondDate = args[3];

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		Date first = null;
		Date second = null;
		try {
			first = formatter.parse(firstDate);
			second = formatter.parse(secondDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		List<Headline> headlines = Headlines.getHeadlines(company, first,
				second);
		Headlines.HeadlinesContainer h = new Headlines.HeadlinesContainer();
		h.headlines = headlines.toArray(new Headline[headlines.size()]);

		CLI.prettyPrint = true;
		JsonOutput.outputToFile(h, fileName, true);
	}

	private static void testRSSParser(String feedURL) {
		RSSFeed feed = RSSParser.downloadFeed(feedURL);

		// Test
		System.out.println("Title: " + feed.getTitle());
		System.out.println("Link: " + feed.getLink());
		System.out.println("Description: " + feed.getDescription());

		for (RSSItem item : feed.getItems()) {
			System.out.println(item.getTitle());
			System.out.println(item.getPubDate());
			System.out.println(item.getLink());
			System.out.println(item.getDescription());
			System.out.println();
		}
		System.out.println("No of articles: " + feed.getItems().size());
	}

}
