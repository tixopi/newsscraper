package scraper;

/**
 * Represent a stream taken from the feedly API See
 * https://developer.feedly.com/v3/streams/ Naming the fields as in the API lets
 * GSON automagically parse (it can be done manually though)
 *
 */
public class FeedlyStream {

	// ** DON'T CHANGE FIELD NAMES ** //

	String id; // feedly id of the stream

	// Optional elements
	long updated; // Timestamp of most recent entry (crawl time) in ms,
					// regardless of newerThan etc
	String title;
	// private String[] alternate; // [0] type, [1] website url

	private FeedlyEntry[] items;

	void prettyPrint() {
		if (items != null) {
			for (FeedlyEntry entry : items) {
				entry.prettyPrint();
				System.out.println();
			}
		} else {
			System.out.println("Error: No entries");
		}
	}

	FeedlyEntry[] getEntries() {
		return items;
	}

}
