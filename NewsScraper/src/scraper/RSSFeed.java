package scraper;

import java.util.ArrayList;
import java.util.Date;

/**
 * Represents an RSS feed. Contains the feed metadata and list of articles.
 *
 */
public class RSSFeed {

	private final String feedURL;
	private String title;
	private String link; // URL to website
	private String description;

	// Optional elements
	private Date lastBuildDate; // Time the content of this feed last changed

	// List of the items in the feed
	private ArrayList<RSSItem> items = new ArrayList<RSSItem>();

	public RSSFeed(String feedUrl) {
		this.feedURL = feedUrl;
	}

	RSSFeed(String feedURL, String title, String link, String description) {
		this.feedURL = feedURL;
		this.title = title;
		this.link = link;
		this.description = description;
	}

	Date getLastBuildDate() {
		return lastBuildDate;
	}

	String getFeedURL() {
		return feedURL;
	}

	String getTitle() {
		return title;
	}

	String getLink() {
		return link;
	}

	String getDescription() {
		return description;
	}

	ArrayList<RSSItem> getItems() {
		return items;
	}

	void addArticle(RSSItem article) {
		items.add(article);
	}

	void setTitle(String title) {
		this.title = title;
	}

	void setLink(String link) {
		this.link = link;
	}

	void setDescription(String description) {
		this.description = description;
	}

	void setLastBuildDate(Date lastBuildDate) {
		this.lastBuildDate = lastBuildDate;
	}

}
