package scraper;

import java.util.Date;

/**
 * Represents an RSS item from the XML feed See
 * http://validator.w3.org/feed/docs/rss2.html for what an RSS item can contain
 */
public class RSSItem {

	// All elements optional, but must have one of title or description
	private String title;
	private String description;
	private String link;
	private String authorEmail;
	private String[] categories;
	private String guid; // Unique identifier for item
	private Date pubDate;

	String getTitle() {
		return title;
	}

	void setTitle(String title) {
		this.title = title;
	}

	String getDescription() {
		return description;
	}

	void setDescription(String description) {
		this.description = description;
	}

	String getLink() {
		return link;
	}

	void setLink(String link) {
		this.link = link;
	}

	String getAuthorEmail() {
		return authorEmail;
	}

	void setAuthorEmail(String authorEmail) {
		this.authorEmail = authorEmail;
	}

	String[] getCategories() {
		return categories;
	}

	void setCategories(String[] categories) {
		this.categories = categories;
	}

	String getGuid() {
		return guid;
	}

	void setGuid(String guid) {
		this.guid = guid;
	}

	Date getPubDate() {
		return pubDate;
	}

	void setPubDate(Date pubDate) {
		this.pubDate = pubDate;
	}

}
