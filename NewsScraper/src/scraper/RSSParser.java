package scraper;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Provides functionality to download an XML file from an RSS feed URL and parse
 * it as DOM document. This can then be converted into an RSSFeed object.
 */
public class RSSParser {

	private static DocumentBuilderFactory factory = DocumentBuilderFactory
			.newInstance();

	// Text for the XML nodes
	private static final String titleTag = "title";
	private static final String linkTag = "link";
	private static final String descripTag = "description";
	private static final String itemTag = "item";
	private static final String pubDateTag = "pubDate";

	// Format for parsing RSS pub dates
	private static DateFormat formatter = new SimpleDateFormat(
			"EEE, dd MMM yyyy HH:mm:ss zzz");

	// Cannot create instances of this
	private RSSParser() {
	}

	/**
	 * Convenience method to download the XML feed file from the URL, parse it
	 * into a DOM document and then into an RSSFeed object. May return null if
	 * error whilst downloading or elsewhere.
	 */
	static RSSFeed downloadFeed(String feedURL) {
		Document doc = downloadAndParse(feedURL);
		if (doc == null)
			return null;
		return parseDOMDoc(doc, feedURL);
	}

	/**
	 * Downloads the XML file from the URL and returns it parsed as a DOM
	 * document. Returns null if there was an error.
	 */
	static Document downloadAndParse(String url) {

		try {
			URL feedUrl = new URL(url);
			return parseXML(feedUrl.openStream());
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}

	}

	/**
	 * Takes an input stream of an XML file and returns it parsed as a document.
	 * Returns null if there was an error parsing. Closes the stream.
	 */
	private static Document parseXML(InputStream input) {
		try {
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document res = builder.parse(input);
			input.close();
			return res;
		} catch (SAXException | IOException | ParserConfigurationException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Parse a DOM document into an RSS feed object.
	 */
	static RSSFeed parseDOMDoc(Document doc, String feedURL) {
		// http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
		// See for what this does
		doc.getDocumentElement().normalize();

		// For RSS 2.0 specification see
		// http://validator.w3.org/feed/docs/rss2.html
		// Basic structure goes rss -> channel -> item (contains the articles)

		// This should be an rss node
		Element rootElement = doc.getDocumentElement();

		// Get the channel node
		Node channelNode = null;
		NodeList nodes = rootElement.getChildNodes();
		for (int i = 0; i < nodes.getLength(); ++i) {
			if (nodes.item(i) instanceof Element
					&& nodes.item(i).getNodeName() == "channel") {
				channelNode = nodes.item(i);
			}
		}

		// Incorrectly formatted XML file
		if (channelNode == null)
			return null;

		// Get child nodes of channel
		// Should include feed metadata and item nodes
		nodes = channelNode.getChildNodes();

		// Loop through and create the RSSFeed object
		RSSFeed feed = new RSSFeed(feedURL);

		for (int i = 0; i < nodes.getLength(); ++i) {
			Node node = nodes.item(i);
			if (nodes.item(i) instanceof Element) {
				switch (node.getNodeName()) {
				case titleTag:
					feed.setTitle(node.getTextContent());
					System.out.println(node.getTextContent());
					break;
				case linkTag:
					feed.setLink(node.getTextContent());
					System.out.println(node.getTextContent());
					break;
				case descripTag:
					feed.setDescription(node.getTextContent());
					System.out.println(node.getTextContent());
					break;
				case itemTag:
					System.out.println("Item found");
					feed.addArticle(parseItem(node));
					break;
				}
			}
		}

		return feed;
	}

	/**
	 * Extract the relevant parts from an item node into an RSSItem
	 *
	 */
	private static RSSItem parseItem(Node item) {

		RSSItem article = new RSSItem();

		NodeList nodes = item.getChildNodes();
		for (int i = 0; i < nodes.getLength(); ++i) {
			Node node = nodes.item(i);
			if (nodes.item(i) instanceof Element) {
				switch (node.getNodeName()) {
				case titleTag:
					article.setTitle(node.getTextContent());
					System.out.println(node.getTextContent());
					break;
				case linkTag:
					article.setLink(node.getTextContent());
					System.out.println(node.getTextContent());
					break;
				case descripTag:
					article.setDescription(node.getTextContent());
					System.out.println(node.getTextContent());
					break;
				case pubDateTag:
					try {
						article.setPubDate(formatter
								.parse("Sat, 24 Apr 2010 14:01:00 GMT"));
						System.out.println(node.getTextContent());
					} catch (ParseException e) {
						e.printStackTrace();
						System.out.println("Couldn't parse pubDate");
					}
					break;
				// TODO set other fields if they exist

				}
			}
		}
		return article;

	}
}
