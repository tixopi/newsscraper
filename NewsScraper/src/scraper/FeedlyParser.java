package scraper;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * Class that interfaces with the feedly cloud API to search for historical RSS
 * feeds and parse the result into a feed object. See
 * https://developer.feedly.com for usage notes of feedly API and
 * https://sites.google.com/site/gson/gson-user-guide for GSON library which is
 * used to parse the JSON returned by feedly.
 */
public class FeedlyParser {

	/** Base URL of feedly API */
	private static final String baseURL = "http://cloud.feedly.com";
	/**
	 * Address to fetch entry ids of a stream Takes parameters streamId (only
	 * required), count (default 20, max 10,000), ranked (newest to oldest,
	 * default newest) newerThan (long timestamp in ms, compares to crawl time
	 * not publish), unreadOnly, and continuation (a continuation string may be
	 * returned if there are more results, in this case passing it as a
	 * parameter will get those results in next query)
	 */
	private static final String entryIdsFromStream = "/v3/streams/ids";
	/**
	 * Address for getting content from stream. Takes similar parameters to
	 * entry ids, count max is only 1,000
	 */
	private static final String contentFromStream = "/v3/streams/contents";
	/** Prepend to RSS feed to get Feedly stream ID */
	private static final String feedlyPrepend = "feed/";

	void getEntries(String feedURL) {

	}

	/**
	 * Grabs the content directly from stream, without getting entry ids first.
	 * Can grab max of 1000 entries. FeedId is RSS feed URL with "feed/"
	 * prepended.
	 * 
	 * @param feedId
	 *            The Feedly streamID
	 * @param count
	 *            Number of articles to fetch
	 * @param newerThan
	 *            Only articles crawled after (or equal) this time will be fetched
	 */
	static FeedlyStream getContentDirectly(String feedId, int count,
			long newerThan) {

		// Download the feed
		InputStream stream = null;
		try {
			URL url = new URL(baseURL + contentFromStream + "?streamId="
					+ URLEncoder.encode(feedId, "UTF-8") + "&count=" + count
					+ "&newerThan=" + newerThan);
			stream = url.openStream();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (stream == null) {
			System.out.println("Error downloading from feedly");
			return null;
		}

		// Get string from the input stream and close the stream
		String jsonString = convertStreamToString(stream);
		// System.out.println(jsonString);

		// Get a map representing the JSON
		// Map<String, Object> jsonMap = parseJSONToMap(jsonString);
		// System.out.println(jsonMap);

		// Get FeedlyStream from the JSON
		FeedlyStream feedlyStream = createFeedlyStream(jsonString);
		// feedlyStream.prettyPrint();

		return feedlyStream;

	}

	/**
	 * Convert a string containing JSON data into a hashmap using the google
	 * GSON library
	 */
	private static Map<String, Object> parseJSONToMap(String jsonString) {
		Map<String, Object> retMap = new Gson().fromJson(jsonString,
				new TypeToken<HashMap<String, Object>>() {
				}.getType());
		return retMap;
	}

	/**
	 * Takes a stream and converts it into a string. Closes the stream.
	 */
	private static String convertStreamToString(InputStream is) {
		// The \A matches the beginning of the string, ie it splits it into one
		// piece
		Scanner s = new Scanner(is);
		s.useDelimiter("\\A");
		String res = s.hasNext() ? s.next() : "";
		s.close();
		return res;
	}

	/**
	 * Create a FeedlyStream filled with the entries from a JSON string using
	 * the GSON library
	 */
	private static FeedlyStream createFeedlyStream(String jsonString) {
		Gson gson = new Gson();
		return gson.fromJson(jsonString, FeedlyStream.class);

	}

}
