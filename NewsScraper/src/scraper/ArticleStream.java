package scraper;

/**
 * Final output of scraper, contains list of articles (feedly metadata and
 * scraped article body). Suitable for writing to file output.
 *
 */
public class ArticleStream {

	public String id; // Feedly stream ID
	public String title; // Feed title
	public long updated; // Timestamp in ms of most recent entry in this stream
	public int numArticles; // Number of articles in this stream

	Article[] articles;

	public ArticleStream(String id, String title, long updated,
			int numArticles, Article[] articles) {
		this.id = id;
		this.title = title;
		this.updated = updated;
		this.numArticles = numArticles;
		this.articles = articles;
	}

	public Article[] getArticles() {
		return articles;
	}

}
