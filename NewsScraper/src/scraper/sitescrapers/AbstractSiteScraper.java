package scraper.sitescrapers;

import java.io.IOException;
import java.net.URL;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 * Abstract class for extracting the article from an URL. Uses the Jsoup HTML
 * parsing library. Concrete implementations should provide site specific code
 * for extracting the article body.
 * 
 */
public abstract class AbstractSiteScraper {

	/** Timeout length to fetch article */
	private static final int DOWNLOAD_TIMEOUT = 3000;

	/**
	 * Fetch the article from the URL
	 * 
	 * @return The article, or "" if there was an error parsing
	 */
	public String scrape(String articleURL) {

		System.out.println("Fetching: " + articleURL);

		Document doc = null;

		// Try to fetch the URL and parse it
		try {
			doc = Jsoup.connect(articleURL).timeout(DOWNLOAD_TIMEOUT).get();
			// Parse using UTF-8
			// doc = Jsoup.parse(new URL(articleURL).openStream(), "UTF-8",
			// articleURL);

			// System.out.println(doc.toString());
			return getArticleFromDoc(doc);
		} catch (IOException e) {
			System.out.println("Error downloading from URL: " + articleURL);
			return "";
			// e.printStackTrace();
		}

	}

	/**
	 * Extract the article from the document
	 * 
	 * @param doc
	 *            The DOM document of the webpage
	 * @return The article, else "" if there was an error
	 */
	abstract String getArticleFromDoc(Document doc);

}
