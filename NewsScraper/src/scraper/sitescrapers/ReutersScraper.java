package scraper.sitescrapers;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Entities;

public class ReutersScraper extends AbstractSiteScraper {

	@Override
	String getArticleFromDoc(Document doc) {

		// Grab the whole article
		// Trying all the possible class names
		Element innerBody = null;
		innerBody = doc.select("#articleText").first();

		if (innerBody != null) {
			// System.out.println(innerBody.text());
			return innerBody.text();
		} else {
			return "";
		}
	}

}
