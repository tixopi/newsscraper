package scraper.sitescrapers;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class BBCScraper extends AbstractSiteScraper {

	/** Class name of div element on BBC site for article body */
	private static final String[] articleBodyClassNames = {
			"story-body__inner", "story-inner", "story-body" };

	@Override
	String getArticleFromDoc(Document doc) {

		// Grab the whole article
		// Trying all the possible class names
		Element innerBody = null;
		int i = 0;
		while (innerBody == null && i < articleBodyClassNames.length) {
			// Try and find element with specified class name
			innerBody = doc.select("." + articleBodyClassNames[i]).first();
			++i;
		}

		if (innerBody != null) {
			System.out.println();
			return innerBody.text();
		} else {
			return "";
		}
	}

}
