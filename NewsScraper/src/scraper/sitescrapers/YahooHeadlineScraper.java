package scraper.sitescrapers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;

import scraper.Headlines.Headline;

public class YahooHeadlineScraper {

	private static final String HEADLINES_CLASS = "yfi_quote_headline";

	public static List<Headline> getHeadlines(Document doc) {

		List<Headline> headlines = new ArrayList<Headline>();
		SimpleDateFormat formatter = new SimpleDateFormat("EEEE, MMMM dd, yyyy");
		Date currentDate = null;

		// Grab the whole article
		// Trying all the possible class names
		Element innerBody = null;
		innerBody = doc.getElementsByClass(HEADLINES_CLASS).first();

		// System.out.println(innerBody.toString());

		for (Node e : innerBody.childNodes()) {
			if (e instanceof Element) {
				Element ei = (Element) e;
				switch(ei.tagName()) {
				case("h3"):
					try {
						currentDate = formatter.parse(ei.text());
//						System.out.println("Formatted " + formatter.format(currentDate));
//						System.out.println("Text " + ei.text());

					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					break;
				case("ul"):
					if(currentDate != null) {
						for(Element l : ei.children()) {
							
							Element link = l.getElementsByTag("a").first();
							Headline h = new Headline(link.text(),link.attr("href"),currentDate);
							headlines.add(h);
							
							
//							System.out.println(l.getElementsByTag("a").first().attr("href"));
						}
						
					}
				}
//				System.out.println(ei.tagName());
			}
			// System.out.println(e.toString());

		}

		return headlines;
	}

}
