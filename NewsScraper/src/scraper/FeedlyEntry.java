package scraper;

/**
 * Class representing a feedly entry. See
 * https://developer.feedly.com/v3/entries/ Naming the fields as in the api
 * allows GSON to automagically create this object from JSON
 *
 */
public class FeedlyEntry {

	private String feedlyId; // Entry id for feedly API
	private long crawled; // Time in ms when feedly crawled the article
	private long published; // Time in ms when article was published

	// Optional elements
	private String title;
	private String originId; // the unique id of this post in the RSS feed (not
								// necessarily an URL!)
	private Link[] canonical; // Canonical URLS?
	private Link[] alternate; // URLs to this article, typically only one
	private Content content; // Likely will have only one of content or summary
	private Content summary;
	private String author;
	private Origin origin; // Contains the streamId, feed title and URL of
							// website
	private String[] keywords;
	private int engagement; // How popular the article is on feedly
	private float engagementRate; // similar to engagement? (doesn't seem to be
									// specified in spec)

	/**
	 * Get the associated URL of this article
	 * 
	 * @return The URL, or "" if no URL attached
	 */
	String getURL() {
		if (originId.startsWith("http")) {
			return originId;
		} else if (canonical != null) {
			return canonical[0].href;
		} else if (alternate != null) {
			return alternate[0].href;
		} else {
			return "";
		}
	}

	String getTitle() {
		return title == null ? "" : title;
	}

	void prettyPrint() {
		System.out.println(title);
		System.out.println(author);
		if (content != null)
			System.out.println(content.content);
		else if (summary != null)
			System.out.println(summary.content);
	}

	// Below are classes representing feedly objects that may be found in the
	// response
	// Having these allows GSON to create them automatically

	/** Represents a link and its media type */
	private static class Link {
		String type;
		String href;
	}

	private static class Content {
		String direction;
		String content;
	}

	private static class Origin {
		String streamId;
		String title;
		String htmlUrl;
	}

}
