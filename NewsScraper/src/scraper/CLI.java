package scraper;

import scraper.Scraper.Feed;

/**
 * Command line argument parser
 *
 */
public class CLI {

	private static String[] args;
	private static int paramCount = 1;
	/** Specified feed to get articles from */
	static Feed feed;
	/** Number of articles to fetch if fetching, default is 20 */
	static int count = 20;
	/**
	 * File name to write output to
	 */
	static String fileOutputName;
	/** Flag indicating whether to strip ArticleStream before output */
	public static boolean strip = false;
	public static boolean prettyPrint = false;

	static void parseArgs(String[] args) {

		if (args.length == 0) {
			System.out
					.println("View the readme for available commands. Type --feeds for availble feeds.");
			System.exit(0);
		}

		CLI.args = args;

		for (int i = 0; i < args.length; ++i) {
			String arg = args[i];
			if (arg.startsWith("--")) {
				// Strip the "--" and set the flag
				setFlag(arg.substring(2), i);
			} else if (arg.startsWith("-")) {
				// Strip the "-" to get the flag(s)
				String flags = arg.substring(1);

				// Loop through and set each flag
				for (char flag : flags.toCharArray()) {
					setFlag(flag, i);
				}
			} else {
				setParam(arg);
			}
		}

		// TODO warn if not all params set, have flag to remove this warning for
		// example if --feeds is called

		// Perform default action with no flags set
		// TODO Check if can write to file etc before downloading everything
		// Scraper.writeArticlesToNewFile(feed, count, fileOutputName);
		Scraper.writeArticlesToFile(feed, count, fileOutputName, true);

	}

	private static void setFlag(String flag, int index) {

		switch (flag) {
		case "feeds":
			// Print out the available feeds
			System.out.println("Available feeds");
			for (Scraper.Feed feed : Scraper.Feed.values()) {
				System.out.println(feed.name());
			}
			break;
		case "count":
			// Set the number of articles to fetch
			try {
				CLI.count = Integer.parseInt(args[++index]);
				if (CLI.count < 1 || CLI.count > 1000)
					throw new NumberFormatException();
			} catch (NumberFormatException e) {
				System.out
						.println("--count: Invalid number - Should be between 1 and 1000");
			}
			break;
		case "stripfile":
			// Strip an existing file
			Scraper.stripFile(args[++index]);
			System.exit(0);
			break;
		case "strip":
			// Set flag to indicate we should strip output
			// TODO This is unsafe to use, will cause problems if appending to
			// an already stripped file
			CLI.strip = true;
			break;
		case "prettyprint":
			CLI.prettyPrint = true;
			System.out.println("Pretty print set");
			break;
		default:
			System.out.println("Command --" + flag + " not recognised");

		}
	}

	private static void setFlag(char flag, int index) {

	}

	private static void setParam(String param) {

		switch (paramCount) {
		case 1:
			// Parse the input into feed
			try {
				feed = Scraper.Feed.valueOf(param);
			} catch (IllegalArgumentException e) {
				System.out.println("Invalid feed specified");
				System.exit(1);
			}
			++paramCount;
			break;
		case 2:
			fileOutputName = param;
			++paramCount;
			break;

		}

	}

}
