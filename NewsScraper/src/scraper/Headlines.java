package scraper;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import scraper.sitescrapers.YahooHeadlineScraper;

public class Headlines {

	private static final String baseURL = "http://finance.yahoo.com/q/h";

	static List<Headline> getHeadlines(String company, Date startDate,
			Date eDate) {

		ArrayList<Headline> headlines = new ArrayList<Headline>();

		Calendar cal = Calendar.getInstance();
		Date currentDate = startDate;
		Date endDate = eDate;

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		System.out.println("Start date: " + formatter.format(currentDate));
		System.out.println("End date: " + formatter.format(endDate));

//		System.out.println(formatter.format(currentDate));

		Document doc = null;
		try {

			while (currentDate.getTime() >= endDate.getTime()) {

				String url = baseURL + "?s=" + company + "&t="
						+ formatter.format(currentDate);
				doc = Jsoup.connect(url).timeout(1000).get();

				List<Headline> hs = YahooHeadlineScraper.getHeadlines(doc);
				if (!hs.isEmpty()) {
					System.out.println("Got headlines from "
							+ formatter.format(hs.get(0).date));
				}
				headlines.addAll(hs);

				// System.out.println(formatter.format(headlines.get(headlines
				// .size() - 1).date));

				// Add one day to last day fetched and set as new current date
				cal.setTime(headlines.get(headlines.size() - 1).date);
				cal.add(Calendar.DATE, -1);
				currentDate = cal.getTime();

				// System.out.println(formatter.format(currentDate));

			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		// Might have gone a few days over, remove extra ones
		filterHeadlines(endDate, headlines);

		// for (Headline h : headlines) {
		// System.out.println(h.headline);
		// System.out.println(formatter.format(h.date));
		// System.out.println();
		// }

		return headlines;

	}

	/**
	 * Remove headlines that occurred before lastDate
	 */
	private static void filterHeadlines(Date lastDate, List<Headline> headlines) {

		Iterator<Headline> it = headlines.iterator();
		while (it.hasNext()) {
			if (it.next().date.getTime() < lastDate.getTime()) {
				it.remove();
			}
		}

	}

	public static class Headline {

		final String headline;
		final String url;
		final Date date;

		public Headline(String headline, String url, Date date) {
			this.headline = headline;
			this.url = url;
			this.date = date;
		}

	}

	// Used so gson gives the array a name
	public static class HeadlinesContainer {
		Headline[] headlines;
	}

}
