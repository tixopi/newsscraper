package scraper;

import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import scraper.output.JsonOutput;
import scraper.sitescrapers.AbstractSiteScraper;
import scraper.sitescrapers.BBCScraper;
import scraper.sitescrapers.ReutersScraper;

/**
 * Class linking together all the different parts
 *
 */
public class Scraper {

	enum Feed {
		BBC, REUTERSCOMPNEWS
	}

	private static EnumMap<Feed, AbstractSiteScraper> siteScraperMap = new EnumMap<Feed, AbstractSiteScraper>(
			Feed.class);
	private static EnumMap<Feed, String> feedlyIDMap = new EnumMap<Feed, String>(
			Feed.class);

	// Add in the site scrapers and Feedly stream IDs for sites
	static {
		siteScraperMap.put(Feed.BBC, new BBCScraper());
		feedlyIDMap
				.put(Feed.BBC,
						"feed/http://newsrss.bbc.co.uk/rss/newsonline_uk_edition/uk/rss.xml");
		siteScraperMap.put(Feed.REUTERSCOMPNEWS, new ReutersScraper());
		feedlyIDMap.put(Feed.REUTERSCOMPNEWS,
				"feed/http://feeds.reuters.com/reuters/companyNews");
	}

	/**
	 * Fetch the articles from the specified feed
	 * 
	 * @param feed
	 *            Feed to fetch from
	 * @param count
	 *            Number of articles to fetch, max 1000
	 * @param newerThan
	 *            Only articles crawled after this time will be fetched
	 */
	public static ArticleStream getArticles(Feed feed, int count, long newerThan) {

		FeedlyStream stream = FeedlyParser.getContentDirectly(
				feedlyIDMap.get(feed), count, newerThan + 1);

		Article[] articles = new Article[stream.getEntries().length];
		int i = 0;
		for (FeedlyEntry entry : stream.getEntries()) {

			// Scrape the article body using the correct site scraper
			// Note articleBody may be "" if error scraping
			String articleBody = siteScraperMap.get(feed)
					.scrape(entry.getURL());
			// System.out.println("Article body: " + articleBody);
			articles[i++] = new Article(entry, articleBody);
			if (articleBody.isEmpty()) {
				System.out.println("Couldn't scrape article body for "
						+ entry.getURL());
			}
			// article.prettyPrint();
		}

		return new ArticleStream(stream.id, stream.title, stream.updated,
				articles.length, articles);

	}

	/**
	 * Calls getArticles with newerThan set to 0
	 * 
	 * @param feed
	 * @param count
	 * @return
	 */
	public static ArticleStream getArticles(Feed feed, int count) {
		return getArticles(feed, count, 0);
	}

	/**
	 * Write articles to a file. If the file doesn't already exists it will be
	 * created. If the file exists and append is set to true, newer articles
	 * than the ones in the file will be fetched and added and the article count
	 * updated. If append is set to false and a file already exists this method
	 * will fail. This method won't add existing articles, ie it only fetches
	 * articles newer than are already stored in the file.
	 * 
	 * @param feed
	 *            Feed to fetch articles from
	 * @param count
	 *            Number of articles to write, max 1000
	 * @param fileName
	 *            File to write to
	 * @param append
	 *            If a file exists and this is true, the file will be updated
	 *            otherwise the method will fail.
	 */
	public static void writeArticlesToFile(Feed feed, int count,
			String fileName, boolean append) {

		ArticleStream oldArticles;

		// Create empty article stream if file doesn't already exist
		if (!Files.exists(Paths.get(fileName))) {
			System.out.println("File didn't already exist - Creating new file");
			oldArticles = new ArticleStream("", "", 0, 0, new Article[0]);
		} else {

			// If file exists but we're not set to append, throw error
			if (!append) {
				System.out.println("Error writing: "
						+ Paths.get(fileName).toString() + " already exists");
				return;
			}

			// Read in existing file
			oldArticles = readObjectFromFile(ArticleStream.class, fileName);
		}

		// Fetch in new articles
		ArticleStream newArticles = getArticles(feed, count,
				oldArticles.updated);

		if (newArticles.getArticles().length == 0) {
			System.out.println("No new articles to fetch");
			return;
		}

		// Create new article stream with concatenation of old and new articles
		// Uses feed id from new articles
		ArticleStream allArticles = new ArticleStream(newArticles.id,
				newArticles.title, newArticles.updated, newArticles.numArticles
						+ oldArticles.numArticles, concat(
						oldArticles.getArticles(), newArticles.getArticles()));

		// Write updated article stream to file, overwriting the existing one
		// Strip file if flag is set
		if (CLI.strip) {
			JsonOutput.outputToFile(stripArticleStream(allArticles), fileName,
					true);
		} else {
			JsonOutput.outputToFile(allArticles, fileName, true);
		}
	}

	/**
	 * Takes a file containing metadata and reduces it to just the article
	 * bodies, then writes it to fileName_stripped
	 * 
	 * @param fileName
	 */
	public static void stripFile(String fileName) {

		// Read from file
		ArticleStream oldArticles = readObjectFromFile(ArticleStream.class,
				fileName);

		if (oldArticles == null) {
			System.out.println("Error stripping file");
			return;
		}

		// Reduce it
		StrippedArticleStream stream = stripArticleStream(oldArticles);

		// Write to file
		JsonOutput.outputToFile(stream, fileName + "_stripped", false);

	}

	/**
	 * Takes an ArticleStream and reduces it to just the article bodies. Also
	 * removes entries that have an empty article body.
	 * 
	 * @param articles
	 */
	public static StrippedArticleStream stripArticleStream(
			ArticleStream articles) {

		List<StrippedArticle> strippedArticles = new ArrayList<StrippedArticle>(
				articles.getArticles().length);

		// Loop through and extract just url, title and body
		// Don't add article if empty body
		for (Article article : articles.getArticles()) {
			// Ignore empty articles
			if (article.articleBody.isEmpty())
				continue;

			strippedArticles.add(new StrippedArticle(article.feedlyEntry
					.getTitle(), article.feedlyEntry.getURL(),
					article.articleBody));

		}

		return new StrippedArticleStream(articles.id, articles.title,
				strippedArticles.size(),
				strippedArticles.toArray(new StrippedArticle[strippedArticles
						.size()]));

	}

	private static <T> T readObjectFromFile(Class<T> objectClass,
			String fileName) {

		// Check if file exists
		if (!Files.exists(Paths.get(fileName))) {
			System.out.println("Error reading file: " + fileName
					+ " doesn't exist");
			return null;
		}

		Gson gson = new Gson();

		// Read in the file
		try {
			return gson.fromJson(Files.newBufferedReader(Paths.get(fileName),
					StandardCharsets.UTF_8), objectClass);
		} catch (JsonSyntaxException | JsonIOException | IOException e) {
			System.out.println("Error reading from existing file " + fileName);
			return null;
			// e.printStackTrace();
		}
	}

	/**
	 * Concatenate lists a and b
	 */
	private static <T> T[] concat(T[] a, T[] b) {
		int aLen = a.length;
		int bLen = b.length;

		@SuppressWarnings("unchecked")
		T[] c = (T[]) Array.newInstance(a.getClass().getComponentType(), aLen
				+ bLen);
		System.arraycopy(a, 0, c, 0, aLen);
		System.arraycopy(b, 0, c, aLen, bLen);

		return c;
	}

	/**
	 * Used for storing reduced version of articles with most metadata removed
	 *
	 */
	private static class StrippedArticleStream {

		final String id; // Feedly stream ID
		final String title; // Feed title
		final int numArticles; // Number of articles in this stream

		StrippedArticle[] articles;

		StrippedArticleStream(String id, String title, int numArticles,
				StrippedArticle[] articles) {
			this.id = id;
			this.title = title;
			this.numArticles = numArticles;
			this.articles = articles;
		}
	}

	private static class StrippedArticle {

		StrippedArticle(String title, String articleURL, String articleBody) {
			this.title = title;
			this.articleURL = articleURL;
			this.articleBody = articleBody;
		}

		final String title;
		final String articleURL;
		final String articleBody;

	}

}
