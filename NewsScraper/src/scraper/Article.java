package scraper;

/**
 * Container class holding a FeedlyEntry, as well as the entire article that was
 * scraped from the URL
 * 
 */
class Article {

	final String articleBody;
	final FeedlyEntry feedlyEntry;

	Article(FeedlyEntry feedlyEntry, String articleBody) {
		this.feedlyEntry = feedlyEntry;
		this.articleBody = articleBody;
	}

	void prettyPrint() {
		feedlyEntry.prettyPrint();
		System.out.println(articleBody);
	}

}
